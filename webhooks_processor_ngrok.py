import requests
import config
import ngrok
import sys
import time
import argparse
import socket


def get_url(method):
        return f"https://api.telegram.org/bot{config.token}/{method}"


def set_webhook(host_url=None):
    files = {}
    if host_url:
        body = {'url': host_url}
    else:
        hostname = socket.gethostname()
        host_url = socket.gethostbyname(hostname)
        body = {'url': f'https://{host_url}:88'}
        files['certificate'] = ('tele.pem', open('/etc/ssl/telegram/tele.pem', 'r'), 'multipart/form-data')
    url = get_url('setwebhook')
    print(body)
    try:
        r = requests.post(url, body, files=files)
        print(f'status: {r} \n response: {r.text}')
    except requests.exceptions.ConnectionError as ex:
        print(f'Telegram server is unreachable. Please, check your VPN connection \n\n\n Error:{ex}')

def get_webhook_info():
    try:
        r = requests.get(get_url('getWebhookInfo'))
        return r.json()
    except requests.exceptions.ConnectionError as ex:
        print(f'Telegram server is unreachable. Please, check your VPN connection \n\n\n Error:{ex}')

def delete_webhook():
    try:
        r = requests.get(get_url('deletewebhook'))
        print(r.text)
    except requests.exceptions.ConnectionError as ex:
        print(f'Telegram server is unreachable. Please, check your VPN connection \n\n\n Error:{ex}')
      

def check_webhook():
    while True:
        ngrok_url = ngrok.get_ngrok_url()
        webhook_url = get_webhook_info()['result']['url']
        if not webhook_url or ngrok_url != webhook_url:
            set_webhook(ngrok_url)
        else:
            break

if __name__ == '__main__':
    while True:
        check_webhook()
        time.sleep(10)