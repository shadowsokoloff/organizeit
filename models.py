from sqlalchemy import (create_engine, 
Column, Integer, String, Text, ForeignKey, Boolean, Time, update, ARRAY)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship
from datetime import time
from config import db_string


engine = create_engine(db_string)
Base = declarative_base()
Session = sessionmaker(bind=engine)
session = Session()

class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    first_name = Column(String)
    tg_id = Column(Integer, unique=True)
    habits = relationship('Habit')

    def __init__(self, tg_id, first_name):
        self.tg_id = tg_id
        self.first_name = first_name
    
    def __repr__(self):
        return f'<User: {self.first_name}, {self.tg_id}>'


class Habit(Base):
    __tablename__ = 'habits'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    description = Column(Text)
    user_id = Column(Integer, ForeignKey('users.id'))
    user = relationship('User', back_populates='habits')
    weekdays = Column(ARRAY(Integer))
    deleted = Column(Boolean)
    reminder_time = Column(Time, default=time(hour=10, minute=00))

    def __init__(self, name, user_id, description=None, weekdays=None, reminder_time=None):
        self.name = name
        self.description = description
        self.user_id = user_id
        self.weekdays = weekdays
        self.reminder_time = reminder_time
    
    def __repr__(self):
        return f'<Habit: {self.name}>\n<Created by: {self.user}>'





#TODO: Разобраться с этой частью. Вероятно, лишнее
#Base.metadata.create_all(engine)