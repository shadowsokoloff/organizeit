from telegram import (Bot, Update, ReplyKeyboardMarkup, 
ReplyKeyboardRemove, InlineKeyboardButton, InlineKeyboardMarkup)
from telegram.error import BadRequest
from telegram.ext import (Dispatcher, CommandHandler, MessageHandler, 
Updater, CallbackQueryHandler, Filters, ConversationHandler)
from flask import Flask, request
import requests
import config
from models import User, session, Habit
from pprint import pprint
import json 
from datetime import time, datetime, timedelta
import logging
import uuid


#logging.basicConfig(format='%(asctime)s - [%(uid)s]- %(name)s - %(levelname)s - %(message)s',
#                    level=logging.DEBUG)
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.DEBUG, filename='/var/log/projects/organizeit_tg_bot/production.log')

logger = logging.getLogger(__name__)


app = Flask(__name__)
bot = Bot(token=config.token)

updater = Updater(token=config.token, use_context=True)
dispatcher = updater.dispatcher

HABITS_ACTION, HABIT_NAME, HABIT_TYPE, TRACKING_TIME, TRACKING_TIME, HABIT_EDIT = range(6)
mon, tue, wed, thu, fri, sat, sun = range(7)

weekdays_dict = {0: 'Понедельник',
    1: 'Вторник', 
    2: 'Среда',
    3: 'Четверг',
    4: 'Пятница',
    5: 'Суббота',
    6: 'Воскресенье'
    }

def start(update, context):
    # Отвечает пользователю на команду /start
    first_name = update.message.chat.first_name
    user_tgid = int(update.message.from_user.id)
    not_exists = session.query(User.id).filter_by(tg_id=user_tgid).scalar() is None
    text = f'Привет, {first_name}! \n\n\nЯ - бот, который поможет тебе следить за распорядком дня, вырабатывать новые привычки,\
избавляться от прокрастинации.\n\n\n\
Пока мой функционал ограничен парой функций, но если тебе понравится работать со мной - мой разработчик будет замотивирован развивать продукт!\
Для того, чтобы добавить привычку введи комманду /habits. Там же ты сможешь увидеть информацию по уже создданным напоминаниям'
    message = context.bot.send_message(chat_id=update.effective_chat.id, text=text)
    logger.debug(f'[START]: отправлено приветственное \
сообщение пользователю {message.chat_id} {first_name} \
пользователь не в БД: {not_exists}')
    if not_exists:
        user = User(user_tgid, first_name)
        session.add(user)
        session.commit()
        logger.debug(f'[CREATE USER]: создан пользователь {user}')


def create_habit(update, context):

    """ Предлагает пользователю указать название привычки """
    
    bot = context.bot
    query = update.callback_query 
    text = f'Введи название привычки, которую ты хочешь отслеживать'
    reply_keyboard = [[InlineKeyboardButton('Отмена', callback_data='cancel')]]
    reply_markup = InlineKeyboardMarkup(reply_keyboard)
    bot.edit_message_text(chat_id=query.message.chat_id,
                            message_id=query.message.message_id,
                            text=text, 
                            reply_markup=reply_markup)
    logger.debug(f'[CREATE HABIT]: Отправлено предложение ввести название привычки \
пользователю {query.message.chat_id}')
    return HABIT_NAME


def set_habit_name(update, context):
    
    """ Метод обработки колбэка с указанием названия привычки.
    Сначала он сохраняет имя привычки и user_id, после выдает 
    экран для выбора способа трекинга привычки """
    
    bot = context.bot
    context.user_data['habit_name'] = update.message.text
    user_id = session.query(User.id).filter(User.tg_id==int(update.message.from_user.id)).one()[0]
    habit = Habit(name=context.user_data['habit_name'], user_id=int(user_id))
    context.user_data['user_id'] = user_id
    context.user_data['habit'] = habit
    session.add(habit)
    session.commit()
    text = 'Способ отслеживания прогресса'
    keyboard = [[InlineKeyboardButton('По дням недели', callback_data='1'),
        InlineKeyboardButton('Отмена', callback_data='cancel')]]
    reply_markup = InlineKeyboardMarkup(keyboard)
    bot.send_message(chat_id=update.effective_chat.id,
                            reply_markup=reply_markup, 
                            text=text)
    logger.debug(f'[SET HABIT NAME]: Задано имя привычки {context.user_data["habit_name"]}')
    return HABIT_TYPE
    

def edit_markup_tracking(update, context):
    # Метод редактирует текст и добавляет клавиатуру к сообщению
    # после получения названия привычки
    bot = context.bot
    query = update.callback_query
    context.user_data['habit_days'] = set()
    text = 'Выбери дни недели:'
    keyboard = [[InlineKeyboardButton('Понедельник', callback_data=mon)],
        [InlineKeyboardButton('Вторник', callback_data=tue)],
        [InlineKeyboardButton('Среда', callback_data=wed)],
        [InlineKeyboardButton('Четверг', callback_data=thu)],
        [InlineKeyboardButton('Пятница', callback_data=fri)],
        [InlineKeyboardButton('Суббота', callback_data=sat)],
        [InlineKeyboardButton('Воскресенье', callback_data=sun)],
        [InlineKeyboardButton('Отмена', callback_data='cancel')],
        [InlineKeyboardButton('Готово!', callback_data='done_days')]
    ]
    reply_markup = InlineKeyboardMarkup(keyboard)
    bot.edit_message_text(chat_id=query.message.chat_id,
                            message_id=query.message.message_id,
                            text=text, 
                            reply_markup=reply_markup)
    logger.debug(f'[EDIT MARKUP TRACING]: Выведено сообщение для выбора \
дней недели для расписания')
    return TRACKING_TIME


def edit_keyboard_markup_days(update, context):
    """ Метод редактирует разметку клавиатуры и текст
    после нажатия на клавишу с определенным днем 
    недели """
    #TODO: переписать с использованием общего метода edit_weekdays_markup
    bot = context.bot
    query = update.callback_query
    keyboard = query.message.reply_markup.inline_keyboard
    callback_data = query.data
    new_keyboard = []
    for row in keyboard:
        for wrapped_row in row:
            row_data = wrapped_row.to_dict()
            if row_data['callback_data'] == callback_data and row_data['callback_data'][-1] != '+':
                new_text = f'✅ {row_data["text"]}'
                new_callback =f'{callback_data}+'
                key = [InlineKeyboardButton(new_text, callback_data=new_callback)]
                context.user_data['habit_days'].add(int(callback_data))
                new_keyboard.append(key)
            elif row_data['callback_data'] == callback_data and row_data['callback_data'][-1] == '+':
                new_text = f'{row_data["text"][1:]}'
                new_callback =f'{callback_data[:-1]}'
                key = [InlineKeyboardButton(new_text, callback_data=new_callback)]
                context.user_data['habit_days'].remove(int(new_callback))
                new_keyboard.append(key)
            else:
                key = [InlineKeyboardButton(row_data['text'], callback_data=row_data['callback_data'])]
                new_keyboard.append(key)
    reply_markup = InlineKeyboardMarkup(new_keyboard)
    bot.edit_message_reply_markup(chat_id=query.message.chat_id,
                                    message_id=query.message.message_id,
                                    reply_markup=reply_markup)
    logger.debug(f'[EDIT KEYBOARD MARKUP DAYS]: \
{query.data}')
    return TRACKING_TIME


def get_time_from_keyboard(update):
    keyboard = update.callback_query.message.reply_markup.inline_keyboard
    hours = int(keyboard[0][1]['callback_data'][:-1])
    minutes = int(keyboard[1][1]['callback_data'][:-1])
    reminder_time = time(hour=hours, minute=minutes)
    logger.debug(f'[GET TIME FROM KEYBOARD]: время напоминания \
        {reminder_time}')
    return reminder_time


def show_keyboard_habit_time(update, context):
    habit = context.user_data['habit']
    weekdays = context.user_data['habit_days']
    if weekdays != []:
        habit.weekdays = weekdays
        session.add(habit)
        session.commit()
    bot = context.bot
    query = update.callback_query
    text = f'установи время, когда бот будет присылать тебе напоминание'
    keyboard = [[InlineKeyboardButton('-', callback_data='hour-'),
                InlineKeyboardButton('9', callback_data='9h'),
                InlineKeyboardButton('+', callback_data='hour+')],
                [InlineKeyboardButton('-', callback_data='minute-'),
                InlineKeyboardButton('00', callback_data='00m'),
                InlineKeyboardButton('+', callback_data='minute+')],
                [InlineKeyboardButton('Отмена', callback_data='cancel'),
                InlineKeyboardButton('Готово!', callback_data='done_time')]]
    reply_markup = InlineKeyboardMarkup(keyboard)
    bot.edit_message_text(chat_id=query.message.chat_id,
                                    message_id=query.message.message_id,
                                    text=text,
                                    reply_markup=reply_markup)
    logger.debug(f'[SHOW KEYBOARD HABIT TIME]: отправка сообщения \
для указания времени привычки {habit}')


def edit_keyboard_habit_time(update, context):
    bot = context.bot
    query = update.callback_query
    keyboard = query.message.reply_markup.inline_keyboard
    callback_hour = datetime.strptime(keyboard[0][1]['callback_data'][:-1], '%H')
    callback_minute = datetime.strptime(keyboard[1][1]['callback_data'][:-1], '%M')
    delta_hour = timedelta(hours=1)
    delta_minute = timedelta(minutes=5)
    if query.data == 'hour+':
        hours = callback_hour + delta_hour
        hours_str = str(hours.hour)
        new_key = InlineKeyboardButton(hours_str, callback_data=f'{hours_str}h')
        keyboard[0][1] = new_key
    elif query.data == 'hour-':
        hours = callback_hour - delta_hour
        hours_str = str(hours.hour)
        new_key = InlineKeyboardButton(hours_str, callback_data=f'{hours_str}h')        
        keyboard[0][1] = new_key
    elif query.data == 'minute+':
        minutes = callback_minute + delta_minute
        minutes_str = str(minutes.minute)
        new_key = InlineKeyboardButton(minutes_str, callback_data=f'{minutes_str}m')
        keyboard[1][1] = new_key
    elif query.data == 'minute-':
        minutes = callback_minute - delta_minute
        minutes_str = str(minutes.minute)
        new_key = InlineKeyboardButton(minutes_str, callback_data=f'{minutes_str}m')
        keyboard[1][1] = new_key
    reply_markup = InlineKeyboardMarkup(keyboard)
    if query.data[0].isdigit():
        query.answer()
    else:
        bot.edit_message_reply_markup(chat_id=query.message.chat_id, message_id=query.message.message_id, reply_markup=reply_markup)
        query.answer()
        logger.debug(f'[EDIT KEYBOARD HABIT TIME]: {query.data}')

        
def cancel(update, context):
    """ Метод для отмены диалога с ботом """
    #TODO: изменить текст и добавить клавишу для перехода в главное меню
    # может быть стоит рассмотреть вариант с переходом в главное меню
    # сразу после отмены
    bot = context.bot
    bot.send_message(text='ты, если шо, заходи', 
                    chat_id=update.effective_chat.id)
    logger.debug(f'[CANCEL]: пользователь {update.effective_chat.id}')
    return ConversationHandler.END


def cancel_callback(update, context):
    query = update.callback_query
    bot.deleteMessage(chat_id=query.message.chat_id,
                        message_id=query.message.message_id)
    new_update = Update(update_id=update.update_id, message=update.callback_query.message)
    logger.debug(f'[CANCEL CALLBACK]: пользователь {query.message.chat_id}')
    start(new_update, context)
    return ConversationHandler.END


def action_type_menu(update, context):
    bot = context.bot
    text = f'Меню работы с привычками'
    keyboard = [[InlineKeyboardButton('Создать привычку', callback_data='create_habit')],
                [InlineKeyboardButton('Список привычек', callback_data='show_habits_list')],
                [InlineKeyboardButton('Отмена', callback_data='cancel')]]
    reply_markup = InlineKeyboardMarkup(keyboard)
    bot.send_message(chat_id=update.effective_chat.id, text=text, reply_markup=reply_markup) 
    logger.debug(f'[ACTION TYPE MENU]: меню редактирования привычек \
пользователя {update.effective_chat.id}')
    return HABITS_ACTION


def show_habits_list(update, context):
    """ Вывод списка привычек пользователя """
    user_id = update.effective_user.id
    bot = context.bot
    query = update.callback_query
    user = session.query(User).filter(User.tg_id==user_id).one()
    habits = session.query(Habit).filter(Habit.user==user).all()
    context.user_data['user_habits'] = habits
    top_ten_habits = habits[:10]
    #TODO: заменить на запрос с вложенным запросом
    keyboard = [[],[]]
    if len(habits) == 0:
        text = 'Привычки еще не заданы'
        query.answer(text)
    else: 
        text = 'Список привычек:\n\n\n'
        text, keyboard = get_habits_list(top_ten_habits)
        navigation_keys = [InlineKeyboardButton('⬅️', callback_data='left.0'),
                            InlineKeyboardButton('➡️', callback_data='right.10')]
        keyboard.append(navigation_keys)
        cancel_button = [InlineKeyboardButton('Отмена', callback_data='cancel')]
        keyboard.append(cancel_button)
        reply_markup = InlineKeyboardMarkup(keyboard)
        bot.edit_message_text(chat_id=query.message.chat_id,
                                    message_id=query.message.message_id,
                                    text=text, reply_markup=reply_markup)
    logger.debug(f'[SHOW HABITS LIST]: вывод привычек пользователя {user}')


def edit_habits_list(update, context):
    habits = context.user_data['user_habits']
    query = update.callback_query
    direction, step = query.data.split('.')
    if direction == 'left' and step == '0':
        query.answer(text='Это начало списка')
    elif direction == 'right':
        ten_habits = habits[int(step):int(step)+10]
        if not ten_habits:
            query.answer('Больше привычек нет')
        else:
            text, keyboard = get_habits_list(ten_habits)
            navigation_keys = [InlineKeyboardButton('⬅️', callback_data=f'left.{step}'),
                        InlineKeyboardButton('➡️', callback_data=f'right.{int(step)+10}')]
            keyboard.append(navigation_keys)
            cancel_button = [InlineKeyboardButton('Отмена', callback_data='cancel')]
            keyboard.append(cancel_button)
            reply_markup = InlineKeyboardMarkup(keyboard)
            bot.edit_message_text(chat_id=query.message.chat_id,
                                        message_id=query.message.message_id,
                                        text=text, reply_markup=reply_markup)
    elif direction == 'left':
        ten_habits = habits[int(step)-10:int(step)]
        text, keyboard = get_habits_list(ten_habits)
        navigation_keys = [InlineKeyboardButton('⬅️', callback_data=f'left.{int(step)-10}'),
                    InlineKeyboardButton('➡️', callback_data=f'right.{int(step)}')]
        keyboard.append(navigation_keys)
        cancel_button = [InlineKeyboardButton('Отмена', callback_data='cancel')]
        keyboard.append(cancel_button)
        reply_markup = InlineKeyboardMarkup(keyboard)
        bot.edit_message_text(chat_id=query.message.chat_id,
                                    message_id=query.message.message_id,
                                    text=text, reply_markup=reply_markup)
    logger.debug(f'[EDIT HABITS LIST]: смена списка привычек {direction}')
   


def get_habits_list(habits):
    keyboard = [[],[]]
    text = 'Список привычек:\n\n\n'
    i = 1
    for habit in habits:
        text = f'{text} {i}. {habit.name}\n'
        key = InlineKeyboardButton(f'{i}', callback_data=f'{habit.id}')
        if i <=5:
            keyboard[0].append(key)
        else:
            keyboard[1].append(key)
        i += 1
    logger.debug(f'[GET HABITS LIST]: список привычек \
{text}')
    return text, keyboard
    

def get_habit_info(update, context):
    bot = context.bot
    query = update.callback_query
    habit_id = query.data
    habit = session.query(Habit).filter(Habit.id==habit_id).one()
    text = f'Привычка {habit.name}\n\n\n\
Дни напоминания: {get_habit_weekdays(habit.weekdays)}\n\
Время напоминания: {habit.reminder_time}'
    keyboard = [[InlineKeyboardButton('Изменить', callback_data=f'edit.{habit_id}'),
                    InlineKeyboardButton('Удалить', callback_data=f'delete.{habit_id}')],
                    [InlineKeyboardButton('Отмена', callback_data='cancel')]]
    reply_markup = InlineKeyboardMarkup(keyboard)
    bot.edit_message_text(chat_id=query.message.chat_id,
                            message_id=query.message.message_id,
                            text=text, reply_markup=reply_markup)
    logger.debug(f'[GET HABIT INFO]: вывод информации о привычке \
{habit}')


def delete_habit(update, context):
    bot = context.bot
    query = update.callback_query
    habit_id = int(query.data.split('.')[1])
    habit = session.query(Habit).filter(Habit.id==habit_id).one()
    habit_name = habit.name
    session.delete(habit)
    session.commit()
    text = f'Привычка "{habit_name}" успешно удалена'
    bot.edit_message_text(chat_id=query.message.chat_id,
                            message_id=query.message.message_id,
                            text=text)
    
    return ConversationHandler.END


def edit_habit(update, context):
    bot = context.bot
    query = update.callback_query
    habit_id = query.data.split('.')[1]
    habit = session.query(Habit).filter(Habit.id==habit_id).one()
    user = session.query(User).filter(User.id==habit.user_id).one()
    context.user_data['habit_name'] = habit.name
    context.user_data['user_id'] = user.id
    context.user_data['reminder_time'] = habit.reminder_time
    context.user_data['habit_id'] = habit_id
    context.user_data['habit'] = habit
    context.user_data['last_func'] = "edit_habit"
    text = f'Чтобы изменить название привычки - пришли новое название.\
Чтобы оставить текущее название - нажми клавишу "Пропустить"\n\n\n\
Текущее название привычки: {habit.name}'
    keyboard = [[InlineKeyboardButton('Отмена', callback_data='cancel'),
                InlineKeyboardButton('Пропустить', callback_data='skip')]]
    reply_markup = InlineKeyboardMarkup(keyboard)
    bot.edit_message_text(chat_id=query.message.chat_id,
                            message_id=query.message.message_id,
                            text=text, reply_markup=reply_markup)
    logger.debug(f'[DELETE HABIT]: удаление привычки {habit}')
    return HABIT_EDIT


def edit_skip_func(update, context):
    func = context.user_data['last_func']
    if func == "edit_habit":
        edit_habit_name(update, context)
    logger.debug(f'[EDIT SKIP FUNC]: нажата клавиша "Пропустить"')
    


def edit_habit_name(update, context):
    bot = context.bot
    habit = context.user_data['habit']
    new_habit_name = update.message.text
    if update.message != None and new_habit_name != habit.name:
        new_habit_name = update.message.text
        habit.name = new_habit_name
        session.add(habit)
        session.commit()
    weekdays = habit.weekdays
    text = 'Выбери дни недели:'
    reply_markup = get_weekdays_keyboard(context, weekdays=weekdays)
    #TODO: Нужно изменить. Удалять сообщение пользователя с названием
    #привычки и редактировать сообщение от бота
    #с предложением ввести имя привычки.
    bot.send_message(chat_id=update.effective_chat.id,
                            message_id=update.effective_message.message_id,
                            text=text,
                            reply_markup=reply_markup)
    logger.debug(f'[EDIT HABIT NAME]: редактирование привычки {habit}')


def get_weekdays_keyboard(context, weekdays=None):
    if weekdays==None:
        keyboard = [[InlineKeyboardButton('Понедельник', callback_data=mon)],
            [InlineKeyboardButton('Вторник', callback_data=tue)],
            [InlineKeyboardButton('Среда', callback_data=wed)],
            [InlineKeyboardButton('Четверг', callback_data=thu)],
            [InlineKeyboardButton('Пятница', callback_data=fri)],
            [InlineKeyboardButton('Суббота', callback_data=sat)],
            [InlineKeyboardButton('Воскресенье', callback_data=sun)],
            [InlineKeyboardButton('Отмена', callback_data='cancel')],
            [InlineKeyboardButton('Готово!', callback_data='done_days')]
        ]
    else:
        keyboard = []
        if not context.user_data.get('habit_days'):
            context.user_data['habit_days'] = set()
        for day_cb, day_desc in weekdays_dict.items():
            if day_cb in weekdays:
                button = [InlineKeyboardButton(f'✅{day_desc}', callback_data=f'{day_cb}+')]
                context.user_data['habit_days'].add(day_cb)
            else:
                button = [InlineKeyboardButton(f'{day_desc}', callback_data=f'{day_cb}')]
            keyboard.append(button)
        keyboard.append([InlineKeyboardButton('Отмена', callback_data='cancel')])
        keyboard.append([InlineKeyboardButton('Готово!', callback_data='done_days')])
    reply_markup = InlineKeyboardMarkup(keyboard)
    logger.debug(f'[GET WEEKDAYS KEYBOARD]: формирование клавиатуры списка дней недели \
{reply_markup}')
    return reply_markup
                

def edit_weekdays_keyboard(update, context):
    bot = context.bot
    query = update.callback_query
    logger.debug(f'[EDIT WEEKDAYS KEYBOARD]: редактирование клавиатуры списка дней недели\
{query}')
    reply_markup = edit_weekdays_markup(context, query)
    bot.edit_message_reply_markup(chat_id=query.message.chat_id,
                                    message_id=query.message.message_id,
                                    reply_markup=reply_markup)
    
    

def edit_weekdays_markup(context, query):
    new_keyboard = []
    keyboard = query.message.reply_markup.inline_keyboard
    callback_data = query.data
    if not context.user_data.get('habit_days'):
        context.user_data['habit_days'] = set()
    for row in keyboard:
        row_data = row[0].to_dict()
        if row_data['callback_data'] == callback_data and row_data['callback_data'][-1] != '+':
            new_text = f'✅ {row_data["text"]}'
            new_callback =f'{callback_data}+'
            key = [InlineKeyboardButton(new_text, callback_data=new_callback)]
            context.user_data['habit_days'].add(int(callback_data))
            new_keyboard.append(key)
        elif row_data['callback_data'] == callback_data and row_data['callback_data'][-1] == '+':
            new_text = f'{row_data["text"][1:]}'
            new_callback =f'{callback_data[:-1]}'
            key = [InlineKeyboardButton(new_text, callback_data=new_callback)]
            context.user_data['habit_days'].remove(int(new_callback))
            new_keyboard.append(key)
        else:
            key = [InlineKeyboardButton(row_data['text'], callback_data=row_data['callback_data'])]
            new_keyboard.append(key)  
    reply_markup = InlineKeyboardMarkup(new_keyboard)
    logger.debug(f'[EDIT WEEKDAYS MARKUP]: редактирование разметки клавиатуры списка дней \
привычки {context.user_date["habit"]}')
    return reply_markup


def edit_habit_time(update, context):
    habit = context.user_data['habit']
    days = sorted(context.user_data['habit_days'])
    habit.weekdays = days
    session.add(habit)
    session.commit()
    bot = context.bot
    query = update.callback_query
    reminder_time = context.user_data['reminder_time']
    text = f'установи время, когда бот будет присылать тебе напоминание'
    hour = reminder_time.hour
    minute = reminder_time.minute
    keyboard = [[InlineKeyboardButton('-', callback_data='hour-'),
                InlineKeyboardButton(f'{hour}', callback_data=f'{hour}h'),
                InlineKeyboardButton('+', callback_data='hour+')],
                [InlineKeyboardButton('-', callback_data='minute-'),
                InlineKeyboardButton(f'{minute}', callback_data=f'{minute}m'),
                InlineKeyboardButton('+', callback_data='minute+')],
                [InlineKeyboardButton('Отмена', callback_data='cancel'), 
                InlineKeyboardButton('Готово!', callback_data='done_time')]]
    reply_markup = InlineKeyboardMarkup(keyboard)
    bot.edit_message_text(chat_id=query.message.chat_id,
                                    message_id=query.message.message_id,
                                    text=text,
                                    reply_markup=reply_markup)
    logger.debug(f'[EDIT HABIT TIME]: редактирование времени оповещения \
о привычке {habit}')


def finish_edit_habit(update, context):
    habit = context.user_data['habit']
    habit.reminder_time = get_time_from_keyboard(update)
    session.commit()
    query = update.callback_query
    weekdays_str = get_habit_weekdays(habit.weekdays)
    text = f'Привычка сохранена.\n\n\n\
Имя: {habit.name}\n\n\
Дни напоминаний: \n{weekdays_str}\n\n\
Время напоминаний: {habit.reminder_time}'
    bot.edit_message_text(chat_id=query.message.chat_id,
                        message_id=query.message.message_id,
                        text=text)
    logger.debug(f'[FINISH EDIT HABIT]: редактирование привычки {habit} \
завершено')

    return ConversationHandler.END



    
def finish_habit_creating(update, context):
    """ Метод записывает привычку в БД """
    query = update.callback_query
    reminder_time = get_time_from_keyboard(update)
    habit = context.user_data['habit']
    habit.reminder_time = reminder_time
    #TODO: create answer after saving habit to db
    session.add(habit)
    session.commit()
    text = f'Создано событие.\n\n\nИмя: {habit.name}\n\n\nДни напоминаний:{get_habit_weekdays(habit.weekdays)}\n\n\n\
Время напоминаний: {habit.reminder_time}'
    bot.edit_message_text(chat_id=query.message.chat_id,
                            message_id=query.message.message_id, text=text)
    logger.debug(f'[FINISH HABIT CREATING]: создание привычки {habit} завершено')
    return ConversationHandler.END


def get_habit_weekdays(weekdays):
    days = sorted(weekdays)
    #print(days)
    text = ''
    for day in days:
        text = f'{text}\n{weekdays_dict[day]}'
    logger.debug(f'[GET HABIT WEEKDAYS]: получение списка дней оповещения \
о привычке {text}')

    return text


def error(update, context):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, context.error)


@app.route('/', methods=['POST'])
def incoming_request():
    update = Update.de_json(request.get_json(force=True), bot)
    logger.info(f'[INCOMING_UPDATE]: {update}')
    if update.effective_chat.type != 'private':
        return 'OK'
    dispatcher.process_update(update)
    dispatcher.add_handler(CommandHandler('start', start))
    conv_handler = ConversationHandler(
        entry_points=[CommandHandler('habits', action_type_menu)],

        states={
            HABITS_ACTION: [CallbackQueryHandler(create_habit, pattern=('create_habit')),
                            CallbackQueryHandler(show_habits_list, pattern=('show_habits_list')),
                            CallbackQueryHandler(edit_habits_list, pattern=('left+?|right+?')),
                            CallbackQueryHandler(get_habit_info, pattern=(r'^\d')),
                            CallbackQueryHandler(edit_habit, pattern='edit*'),
                            CallbackQueryHandler(delete_habit, pattern='delete*')],
            HABIT_EDIT: [MessageHandler(Filters.text, edit_habit_name),
                         CallbackQueryHandler(edit_weekdays_keyboard, pattern=(r'\d?\+?$')),
                         CallbackQueryHandler(edit_habit_time, pattern='done_days'),
                         CallbackQueryHandler(edit_keyboard_habit_time, pattern=(r'minute+?|hour+?|\d[h|m]')),
                         CallbackQueryHandler(finish_edit_habit, pattern='done_time'),
                         CallbackQueryHandler(edit_skip_func, pattern='skip')],
            HABIT_NAME: [MessageHandler(Filters.text, set_habit_name)],
            HABIT_TYPE: [CallbackQueryHandler(edit_markup_tracking, pattern='1')],
            TRACKING_TIME: [CallbackQueryHandler(edit_keyboard_markup_days, pattern=(r'\d?\+?$')),
                                CallbackQueryHandler(edit_keyboard_habit_time, pattern=(r'minute+?|hour+?|\d+[h|m]')),
                                CallbackQueryHandler(show_keyboard_habit_time, pattern=('done_days')),
                                CallbackQueryHandler(finish_habit_creating, pattern=('done_time'))]

        },

        fallbacks=[CommandHandler('cancel', cancel),
                    CallbackQueryHandler(cancel_callback, pattern='cancel')],
        conversation_timeout=180
    )
    dispatcher.add_handler(conv_handler)

    dispatcher.add_error_handler(error)

    return 'OK'


