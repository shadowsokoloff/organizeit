from models import *
from datetime import datetime, time, timedelta, date

def generate_dt():
    dt_now = datetime.combine(date.today(), time(0,0))
    dt_tomorrow = datetime.combine(date.today(), time(0,0)) + timedelta(days=1)
    dt_delta = timedelta(minutes=1)
    dt_list = []
    while True:
        dt_now = dt_now + dt_delta
        if dt_now < dt_tomorrow:
            dt_list.append(dt_now)
        else:
            break
    return dt_list


def add_dt_to_db(dt_list):
    user = session.query(User).filter(User.id==4).one()
    weekdays = range(7)
    for dt in dt_list:
        name = dt.strftime("%H:%M")
        t = time(hour=dt.hour, minute=dt.minute)
        habit = Habit(name=name, user_id=user.id, weekdays=weekdays, reminder_time=t)
        session.add(habit)
    session.commit()

if __name__ == ('__main__'):
    dt = generate_dt()
    add_dt_to_db(dt)
    