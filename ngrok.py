import argparse
import os 
import requests
import time

def start_ngrok():
    os.system("screen -dmS ngrok ngrok http 5000 --region eu")
    print('started Ngrok')

def stop_ngrok():
    os.system("screen -X -S ngrok quit")
    print('stoped Ngrok')

def status_ngrok():
    log = os.system('cat screenlog.0')
    print(log)

def get_ngrok_url():
    host_url = ''
    while True:
        try:
            host_url = requests.get('http://localhost:4040/api/tunnels').json()['tunnels'][0]['public_url']
            if is_https(host_url):
                break
        except requests.exceptions.ConnectionError:
            start_ngrok()
            time.sleep(5)
    return host_url
        
def is_https(url):
    if 'https' in url:
        return True
    else:
        return False

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Management utility for ngrok')
    parser.add_argument('command', metavar='C', type=str, nargs='?', help='Command: [start|stop]', default='status', choices=['start', 'stop'])
    args = parser.parse_args()
    command = args.command
    if command == 'start':
        start_ngrok()
    elif command == 'stop':
        stop_ngrok()

