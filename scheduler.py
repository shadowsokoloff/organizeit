from models import * 
from datetime import datetime, date, time, timedelta
from telegram import Bot
from telegram.error import BadRequest
import config


bot = Bot(token=config.token)


def get_habits():
    dt_now = datetime.now()
    time_now = time(hour=dt_now.hour, minute=dt_now.minute)
    habits = session.query(Habit).filter(Habit.reminder_time==time_now).all()
    return habits

def send_notifications(habits):
    today = datetime.now().weekday()
    for habit in habits:
        if today in habit.weekdays:
            user = session.query(User).filter(User.id==habit.user_id).one()
            text = f'Напоминание: {habit.name}'
            try:
                bot.send_message(text=text, chat_id=user.tg_id)
            except BadRequest as bad_request:
                print(bad_request)
        else:
            continue


if __name__ == ("__main__"):
    habits = get_habits()
    send_notifications(habits)